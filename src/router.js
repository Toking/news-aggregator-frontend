import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home'
import About from './views/About'
import Profile from './views/Profile'
import Index from './views/Index'
import Settings from './views/settings/Settings'
import Contacts from './views/Contacts'
import Team from './views/Team'
import Rating from './views/Rating'
import ModeratorPage from './views/ModeratorPage'
import TopicMaker from './views/TopicMaker'
import Confirmation from './views/Confirmation'
import OneNews from './views/OneNews'
import Pricing from './views/Pricing'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name : 'index',
      component: Index
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/contacts',
      name: 'contacts',
      component: Contacts
    },
    {
      path: '/team',
      name: 'team',
      component: Team
    },
    {
      path: '/pricing',
      name: 'pricing',
      component: Pricing
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings
    },
    {
      path: '/rating',
      name: 'rating',
      component: Rating
    },
    {
      path: '/topic-maker',
      name: 'topic-maker',
      component: TopicMaker
    },
    {
      path: '/moderator',
      name: 'moderator',
      component: ModeratorPage
    },
    {
      path: '/confirm',
      name: 'confirm',
      component: Confirmation,
      props: (route) => ({ confirmationToken: route.query.confirmationToken })
    },
    {
      path: '/one_news',
      name: 'one_news',
      component: OneNews
    }
  ]
})
