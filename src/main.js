import Vue from 'vue'
import Vuelidate from 'vuelidate'
import VueAxios from 'vue-axios'
import App from './App.vue'
import wysiwyg from "vue-wysiwyg";
import VueSSE from 'vue-sse';


import router from './router'
import store from './store'
import createAxios from './config/http'

import '../node_modules/bulma/bulma.sass'
import './assets/style.css'

const axios = createAxios(store)

Vue.config.productionTip = false

Vue
  .use(Vuelidate)
  .use(VueAxios, axios)
  .use(VueSSE)
  .use(wysiwyg, {});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');