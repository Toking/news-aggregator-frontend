import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex)

const vuexPersist = new VuexPersist({
    key: 'my-app',
    storage: localStorage
})

export default new Vuex.Store({
  state: {
    token: '',
    loggedIn: false,
    user: {
        id: '',
        username: '',
        email: '',
        pictureUrl: "https://virtual-strategy.com/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png",
        subscriptionType: '',
        yp: '',
        isConfirmed: false,
        registrationDate: ''
    },
    news: {
        id: "",
        title: "",
        link: "",
        category: "",
        value: "",
        date: ""
    }
  },
  mutations: {
      //Token setting
      setToken(state,data) {
          state.token = data;
      },

      //User data setting
      setUserRegistrationDate(state,data) {
          state.user.registrationDate = data;
      },
      setUserID(state,data) {
          state.user.id = data;
      },
      setUsername(state,data) {
          state.user.username = data;
      },
      setEmail(state,data) {
          state.user.email = data;
      },
      setPictureUrl(state,data) {
          state.user.pictureUrl = data;
      },
      setSubscriptionType(state,data) {
          state.user.subscriptionType = data;
      },
      setYP(state,data) {
          state.user.yp = data;
      },

      //User session state
      login (state) {
          state.loggedIn = true;
      },
      logout (state) {
          state.loggedIn = false;
          state.token = "";
      },

      //News info setting
      setNewsId(state,data) {
          state.news.id = data;
      },
      setNewsTitle(state,data) {
          state.news.title = data;
      },
      setNewsLink(state,data) {
          state.news.link = data;
      },
      setNewsCategory(state,data) {
          state.news.category = data;
      },
      setNewsValue(state,data) {
          state.news.value = data;
      },
      setNewsDate(state,data) {
          state.news.date = data;
      },
  },
  actions: {

  },
  plugins: [vuexPersist.plugin]
})

