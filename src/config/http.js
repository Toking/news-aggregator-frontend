// read more: https://github.com/mzabriskie/axios
// includes info from them
import axios from 'axios'

axios.defaults.headers.common['Content-Type'] = 'application/json'

const apiURL = process.env.VUE_APP_API_URL
const baseConfig = {
    baseURL: `${apiURL}/api/v1`,
    timeout: 20000,
    headers: {},
    withCredentials: true
}

export default function createAxios (store) {
    const http = axios.create(baseConfig)

    // append auth header
    http.interceptors.request.use(config => {
        const { token } = store.state

        if (token) {
            config.headers.authorization = `Bearer ${token}`
        }

        return config
    })

    return http
}
