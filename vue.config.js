module.exports = {
    devServer: {
        proxy: {
            '/api/v1': {
                target: 'http://dev.server.01.yourpoint.io:8080',
                ws: true,
                changeOrigin: true
            }
        }
    }
};
