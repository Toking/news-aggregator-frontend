# All

<h3>1) Log in :</h3>
  params from front: 
    - Login (mail),
    - password
  response from back: 
    - user id;
    - user name;
    - user email;
    - token;
    - user info: { status, raiting, ...}
   
<h3>2) Log out:</h3>
  params from front: 
    - user id
  response from back: 
    - 
<h3>3) Sign up:</h3>
  params from front: 
    - user name;
    - password;
    - birthday;
    - email 
  response from back: 
    -  ?
<h3>4) Send opinion about news:</h3>
  params from front: 
    - user id;
    - news id;
    - opinion message;
  response from back: 
    - 
<h3>5)  Get opinions by news id:</h3>
  params from front: 
    - news id;
    - period of time (?)
  response from back: 
    - opinions array
<h3>6)  Get news by topic :</h3>
  params from front: 
    - topic id/name;
    - period of time (?)
  response from back: 
    - news array [{ title, body, date/time}]
<h3>7) Get current topics:</h3>
  params from front: 
    - 
  response from back: 
    - topics array
<h3>8)  Get user's news/opinions by user id</h3>
  params from front: 
    - user id
  response from back: 
    - news array: [
      {
        newsId, title, time, message, userOpinionsAboutThisNews...
      }
    ]
<h3>9) Get prices:</h3>
  params from front: 
    - 
  response from back: 
    - price categoties array
<h3>10)  Password recovery:</h3>
  params from front: 
    - mail
  response from back: 
    - not exist/mail was sent

