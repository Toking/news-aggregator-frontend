# User

<h3>1) Log in :</h3>
  params from front: 
    - Login (mail),
    - password
  response from back: 
    - user id;
    - user name;
    - user email;
    - token;
    - user info: { status, raiting, ...}
   
<h3>2) Log out:</h3>
  params from front: 
    - user id
  response from back: 
    - 
<h3>3) Sign up:</h3>
  params from front: 
    - user name;
    - password;
    - birthday;
    - email 
  response from back: 
    -  ?
<h3>4)  Get user's news/opinions by user id</h3>
  params from front: 
    - user id
  response from back: 
    - news array: [
      {
        newsId, title, time, message, userOpinionsAboutThisNews...
      }
    ]
<h3>5)  Password recovery:</h3>
  params from front: 
    - mail
  response from back: 
    - not exist/mail was sent

